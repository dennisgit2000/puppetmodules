# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include profile::profile02
class profile::profile02 {

 package { 'nginx':
  ensure  => present,
}

 service { 'nginx':
  ensure  => running,
}

}

