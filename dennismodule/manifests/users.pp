# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include dennismodule::users
class dennismodule::users {
 
 group { 'devops' :
  ensure     => present,
  gid        => 5000,
 }
 user{ 'warren':
  ensure     => present,
  managehome => true,
  uid        => 5501,
  groups     => 'devops',
}
 user { 'david':
  ensure     => present,
  managehome => true,
  uid        => 5502,
  groups     => 'devops',
}
 user { 'ellen':
  ensure     => present,
  managehome => true,
  uid        => 5503,
  groups     => 'devops',
}

}
